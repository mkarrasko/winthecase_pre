<?php
include "./inc/header.php";
include "./inc/connect.php";
?>

<main class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-10 col-lg-9"> 
            <form id="formulario_code" class="wss_form_container w-100 p-4" style='' action="" method="post">
                <div id="header-form" class="border-bottom text-center mb-4">
                    <h2 class="textoColorSecundario">CÓDIGO</h2>
                    <p>Para acceder es necesario que introduzca un código.</p>
                </div>

                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 col-lg-9">
                        <div class="form-group">
                            <input type="text" id="wss_code" name="wss_code" class="wss_form form-control" placeholder="Introduzca un código" />
                            <!-- <label class="wss_label textoColorPrincipal" for="wss_code">Código</label> -->
                            <!-- <select id="wss_code" name="wss_code" class="wss_form form-control">
                                <option value="">-- Seleccione un código --</option>
                                <option value="PEDIACASE1">PEDIACASE1</option>
                                <option value="PEDIACASE2">PEDIACASE2</option>
                                <option value="PEDIACASE3">PEDIACASE3</option>
                                <option value="PEDIACASE4">PEDIACASE4</option>
                            </select> -->
                        </div>
                    </div>
                    <div class="my-4" style='width: 100%; position: relative; overflow: hidden;'>
                        <div class="row">
                            <div class="col-12 text-center">
                                <button id="submituserCode" class="btn btn-outline-primary mx-1 my-0 wss_btEnviar">ENVIAR</button> 
                            </div>
                        </div>
                    </div>
                    <div class="mensaje"></div>
                </div>
            </form>
        </div>
    </div>
</main>

<?php
include "./inc/footer.php";
?>